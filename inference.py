'''
推理
解码
'''
from data import get_data
from model import get_model
from fastNLP.core import DataSetIter
def main():
    data_bundle = get_data()
    model = get_model(data_bundle, restore_model)
    data_iter=DataSetIter(data_bundle.get_dataset('test'))
    for batch_x,batch_y in data_iter:
        pred = model.predict(batch_x,len(batch_x[0]))
        