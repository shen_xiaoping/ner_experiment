from fastNLP.modules import ConditionalRandomField
from fastNLP.core.const import Const as C
from torch.nn import functional as F
from fastNLP.core.utils import seq_len_to_mask
from fastNLP.modules.decoder.crf import allowed_transitions
from fastNLP.embeddings import BertEmbedding
from fastNLP.models import BiLSTMCRF
import torch
from fastNLP.models.base_model import BaseModel
from torch import nn


def get_embed(data_bundle):
    embed = BertEmbedding(vocab=data_bundle.get_vocab(
        'words'), model_dir_or_name='cn-wwm-ext')
    return embed


def freeze(embed, flag=True):
    if flag:
        # 固定模型参数
        for i, (name, param) in enumerate(embed.named_parameters()):
            tmp = name.split('.')[4]
            if i < 5:
                param.requires_grad_(False)
                # print(name, param.requires_grad)
            elif tmp.isdigit() and int(tmp) < 8:  # freeze layer 1~12可调
                param.requires_grad_(False)
                # print(name, param.requires_grad)
            else:
                # print(name, param.requires_grad)
                pass


def restore(model, restore_model=None):
    if restore_model:
        print('载入本地模型', restore_model)
        model.load_state_dict(torch.load(restore_model))


def _model_pipeline(get_model):

    def fun(data_bundle, restore_model=None):
        # 词嵌入
        embed = get_embed(data_bundle)
        freeze(embed)
        model = get_model(embed, data_bundle)
        # 本地模型恢复
        restore(model, restore_model)
        return model
    return fun


class Bert_Seq_Classification(BaseModel):
    r"""
    结构为embedding  + FC + Dropout + CRF.

    """

    def __init__(self, embed, num_classes, dropout=0.5,
                 target_vocab=None):
        r"""

        :param embed: 支持(1)fastNLP的各种Embedding, (2) tuple, 指明num_embedding, dimension, 如(1000, 100)
        :param num_classes: 一共多少个类
        :param dropout: dropout的概率，0为不dropout
        :param target_vocab: Vocabulary对象，target与index的对应关系。如果传入该值，将自动避免非法的解码序列。
        """

        super().__init__()
        self.embed = embed
        self.dropout = nn.Dropout(dropout)
        self.fc = nn.Linear(self.embed.embedding_dim, num_classes)

        trans = None
        if target_vocab is not None:
            assert len(
                target_vocab) == num_classes, "The number of classes should be same with the length of target vocabulary."
            trans = allowed_transitions(
                target_vocab.idx2word, include_start_end=True)

        self.crf = ConditionalRandomField(
            num_classes, include_start_end_trans=True, allowed_transitions=trans)

    def _forward(self, words, seq_len=None, target=None):
        words = self.embed(words)
        feats = self.fc(words)
        feats = self.dropout(feats)
        logits = F.log_softmax(feats, dim=-1)
        # return {'pred':logits}
        mask = seq_len_to_mask(seq_len)
        if target is None:
            pred, _ = self.crf.viterbi_decode(logits, mask)
            return {C.OUTPUT: pred}
        else:
            loss = self.crf(logits, target, mask).mean()
            return {C.LOSS: loss}

    def forward(self, words, seq_len, target):
        return self._forward(words, seq_len, target)

    def predict(self, words, seq_len):
        return self._forward(words, seq_len)


@_model_pipeline
def get_model1(embed, data_bundle):
    # 输入模型
    model = BiLSTMCRF(embed=embed,
                      num_classes=len(data_bundle.get_vocab('target')),
                      num_layers=1, hidden_size=100, dropout=0.2,
                      target_vocab=data_bundle.get_vocab('target'))
    return model


@_model_pipeline
def get_model2(embed, data_bundle):
    model = Bert_Seq_Classification(
        embed, len(data_bundle.get_vocab('target')), dropout=0.4, target_vocab=data_bundle.get_vocab('target'))
    return model


get_model = get_model2

if __name__ == "__main__":
    from fastNLP.io import WeiboNERPipe
    import config as CONFIG
    data_bundle = WeiboNERPipe().process_from_file(CONFIG.Path.data)
    data_bundle.rename_field('chars', 'words')  # bert需要的头是words
    get_model(data_bundle)
    pass
