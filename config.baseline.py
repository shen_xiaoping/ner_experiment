from config import os, osp, Path, Train, Test
Path.tag.name = osp.basename(__config__).split('.')[1]
Path.pjoin()
Path.enable()
Train.epochs = 20
# Train.lr = 4e-5