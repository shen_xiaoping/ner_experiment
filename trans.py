from transformers import BertTokenizer, BertPreTrainedModel
from torch.nn import Conv1d, Dropout, Linear
from torch.nn import Module


class Model(Module):
    def __init__(self, sel_len, dropout_p=0.2, num_classes=5):
        super(Model, self).__init__()
        # roberta也用
        self.embed = BertPreTrainedModel.from_pretrained(
            './fast_data/pretrain_model/pytorch/chinese_roberta_wwm_ext/')  # 可以用state_dict来手动添加层
        self.embed.train()
        print(self.embed.__dict__)
        self.dropout = Dropout(dropout_p)
        self.context = Conv1d(sel_len, sel_len, 3, 1, 1)
        self.fc = Linear(self.embed.hidden_size, num_classes)
        
    def forward(self, input):

        return


if __name__ == "__main__":
    m = Model()
