from config import os, osp, Path, Train, Test
Path.tag.name = osp.basename(__config__).split('.')[1]
Path.data["train"] = './mydata/train_ext.data'
Path.pjoin()
Path.enable()
Train.epochs = 6
# Train.lr = 4e-5
