搭自己的bert pipeline  
要用到  
fastnlp0.5.0  
# 数据总览
In total 3 datasets:  
        train has 360 instances.  
        dev has 20 instances.  
        test has 39 instances.  
In total 2 vocabs:  
        chars has 1524 entries.  
        target has 9 entries.  
# 运行 
## 普通
python main.py
## 分布式运行
>>CORES=`lscpu | grep Core(s) | awk '{print $4}'`  
export OMP_NUM_THREADS=$CORES  
python -m torch.distributed.launch --nproc_per_node=2 main.py --dist

