import config as CONFIG
from fastNLP.io import WeiboNERPipe

def get_data():
    data_bundle = WeiboNERPipe().process_from_file(CONFIG.Path.data)
    print('看看数据总结\n', data_bundle, sep='')
    data_bundle.rename_field('chars', 'words')  # bert需要的头是words
    return data_bundle

if __name__ == "__main__":
    data_bundle = get_data()
    print(data_bundle)
