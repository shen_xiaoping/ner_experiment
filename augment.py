'''
数据增强
- 记录所有实体
- 随机替换
- 修改数字
- 修改字母
'''
import json
import random
import re
from operator import add
from functools import reduce


def read_data(filepath):
    with open(filepath, 'r') as f:
        lines = f.readlines()
        lines.append('\n')  # 换行后没内容居然不读了
    return lines


def replace_num(lines):
    # 修改数字
    cn_pattern = re.compile('[\u4e00-\u9fa5]')
    for i, l in enumerate(lines):
        if len(l) > 1:
            if l[0].isnumeric() and not cn_pattern.match(l[0]):
                print(i, l)
                lines[i] = '0'+lines[i][1:]


def replace_letter(lines):
    # 修改字母
    pattern = re.compile(r'[a-zA-Z]')
    for i, l in enumerate(lines):
        if len(l) > 1:
            if pattern.match(l[0]):
                lines[i] = 'x'+lines[i][1:]
                print(l)


def entity_dict(lines, wirte_flag=False):
    # 记录所有的实体位置
    # return: vocab, dict
    vocab = {}
    start = -1
    start_tag = 'O'
    for i, l in enumerate(lines):
        l = l.strip()
        if len(l) > 0:
            char, tag = l.split()
            if tag != 'O':
                tag = tag[2:]
                if tag not in vocab:
                    vocab[tag] = []
            if tag == start_tag:
                # 连续时，就继续
                continue
            else:
                if start!= -1:
                    #  之前已经在记录，就保存下来
                    entity = ''.join([lines[k][0] for k in range(start, i)])
                    vocab[start_tag].append((start, i, entity))
                if tag == 'O':
                    # 如果进入负例，则关闭记录
                    start = -1
                else:
                    # 否则开始新的记录，解决连续标签切换问题
                    start = i 
                start_tag = tag  # 使用新的哨兵
    if wirte_flag:
        json.dump(vocab, open('vocab.json', 'w'), indent=2, ensure_ascii=False)
    return vocab


def replace_entity(lines: list, vocab: dict, k: int = 20):
    N_times = []  # 存100//k倍的训练集
    current_lines = lines.copy()
    # 对每个类别都做k次替换
    for _ in range(100//k):
        current_lines = []
        for tag, v in vocab.items():
            exchanges = random.choices(v, k=k)
            exchanges.sort(key = lambda e: e[0])
            left = 0
            for e in exchanges:
                current_lines.append(lines[left:e[0]])  # 左段
                target = random.choice(v)
                current_lines.append(lines[target[0]:target[1]])  # 替换后的实体
                left = e[1]
                print(e, '-->', target)
            current_lines.append(lines[left:])  # 最后一段
        current_lines = reduce(add,current_lines)
        N_times.append(current_lines)
        print('------------'*3)
    return N_times


def write_data(lines: list, filename: str):
    if isinstance(lines[0], str):
        with open(filename, 'w') as f:
            f.writelines(lines)
        print('ok')
    elif isinstance(lines[0], list):
        with open(filename, 'w') as f:
            f.writelines(reduce(add, lines))
        print('ok')
