import os
from os import path as osp
'''
用来做自动补全
'''


class Path():
    class tag():
        name = osp.basename(__file__).split('.')[0]
        model_postfix = '.model'  # saved_model/<tag>.*.model_postfix
        log_postfix = '.log'
        result_postfix = '.result'
    workdir = './big_data/myNER'
    basepath = osp.join(workdir, tag.name)
    saved_model = osp.join(basepath, '{}'+tag.model_postfix)
    log = osp.join(basepath, '{}'+tag.log_postfix)
    result = osp.join(basepath, '{}'+tag.result_postfix)
    data = {"train": './mydata/train_alias.data',
            "dev": './mydata/dev_alias.data', "test": './mydata/test_alias.data'}
    restored_model = osp.join(workdir, 'restore.model')

    def enable():
        if not osp.isdir(Path.basepath):
            print('新建文件夹', Path.basepath)
            os.mkdir(Path.basepath)
        for k, v in Path.data.items():
            try:
                assert osp.isfile(v)
            except AssertionError as e:
                print(e, v)
                quit()

    def pjoin():
        basepath = osp.join(Path.workdir, Path.tag.name)
        Path.basepath = basepath
        Path.saved_model = osp.join(basepath, '{}'+Path.tag.model_postfix)
        Path.log = osp.join(basepath, '{}'+Path.tag.log_postfix)
        Path.result = osp.join(basepath, '{}'+Path.tag.result_postfix)


class Train():
    epochs = 20
    start_epoch = 0
    batch_size = 16
    max_len = 300
    hidden_unit_nums = 100
    lr = 1e-4


class Test():
    batch_size = 64
