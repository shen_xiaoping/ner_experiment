import argparse
import torch.distributed as torch_dist

from fastNLP import SpanFPreRecMetric
from torch.optim import Adam, SGD, AdamW
from fastNLP import LossInForward
import config as CONFIG
from fastNLP import Trainer
from fastNLP import Tester
import torch
from fastNLP import DistTrainer, get_local_rank
from model import get_model
from data import get_data


def train():
    # 初始化分布式进程
    if dist:
        torch_dist.init_process_group('nccl')
    if dist and get_local_rank() != 0:
        torch_dist.barrier()  # 先让主进程(rank==0)先执行，进行数据处理，预训模型参数下载等操作，然后保存cache
    # 载入原始数据
    # TODO 数据预处理，数字和型号单词
    data_bundle = get_data()

    # 模型
    model = get_model(data_bundle, restore_model)
    # 训练
    metric = SpanFPreRecMetric(tag_vocab=data_bundle.get_vocab('target'))
    optimizer = AdamW(model.parameters(), lr=CONFIG.Train.lr)
    loss = LossInForward()
    device_count = torch.cuda.device_count()
    device = list(range(device_count)) if device_count > 0 else 'cpu'
    print('device', device)
    # lr策略、早停、日志
    if dist and get_local_rank() == 0:
        torch_dist.barrier()  # 主进程执行完后，其余进程开始读取cache
    if not dist:
        # model = torch.nn.DataParallel(model)
        device = None if isinstance(model, torch.nn.DataParallel) else 0
        device_count = 2 if isinstance(model, torch.nn.DataParallel) else 1
        trainer = Trainer(data_bundle.get_dataset('train'), model,
                          loss=loss, optimizer=optimizer,
                          batch_size=CONFIG.Train.batch_size * device_count,
                          num_workers=4*device_count,
                          n_epochs=min(CONFIG.Train.epochs, min_epoch),
                          dev_data=data_bundle.get_dataset("dev"),
                          dev_batch_size=64,
                          metrics=metric, device=device)
    else:
        trainer = DistTrainer(
            train_data=data_bundle.get_dataset("train"),
            dev_data=None,
            model=model,
            loss=loss,
            metrics=metric,
            optimizer=optimizer,
            num_workers=4*device_count,
            # callbacks_master=callbacks_master,  # 仅在主进程执行（如模型保存，日志记录）
            # callbacks_all=callbacks_all,    # 在所有进程都执行（如梯度裁剪，学习率衰减）
            batch_size_per_gpu=CONFIG.Train.batch_size,  # 指定每个GPU的batch大小
            n_epochs=min(CONFIG.Train.epochs, min_epoch),
        )
    if CONFIG.Train.epochs < min_epoch:
        trainer.train()
        # 测试
        tester = Tester(data_bundle.get_dataset('test'), model, metrics=metric)
        tester.test()
    else:
        L = CONFIG.Train.epochs//min_epoch
        for _ in range(L):
            print('{}/{}'.format(_, L))
            trainer.train()
            # 测试
            tester = Tester(data_bundle.get_dataset(
                'test'), model, metrics=metric)
            tester.test()
    # 记录指标
    if not dist or (dist and trainer.is_master):
        torch.save(model.state_dict(), CONFIG.Path.restored_model)

    pass


if __name__ == "__main__":
    min_epoch = 1
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', default='config.baseline.py')
    parser.add_argument('--restore-model', default=None)
    parser.add_argument('--dist', action='store_true',
                        default=False)
    parser.add_argument("--local_rank", type=int, default=0)
    parser.add_argument("--local_world_size", type=int, default=1)
    args = parser.parse_args()
    restore_model = args.restore_model  # 恢复模型的路径
    dist = args.dist
    print('当前配置', args.config)
    exec("__config__ = '{}' \n".format(
        args.config) + open(args.config).read())  # 执行diff配置
    train()
'''
分布式启动命令
python -m torch.distributed.launch --nproc_per_node=2 main.py --args
2是卡数量
'''
